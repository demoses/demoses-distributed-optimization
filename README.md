## Badges

(Customize these badges with your own links, and check https://shields.io/ or https://badgen.net/ to see which other badges are available.)

| fair-software.eu recommendations | |
| :-- | :--  |
| (1/5) code repository              | [![github repo badge](https://img.shields.io/badge/github-repo-000.svg?logo=github&labelColor=gray&color=blue)](https://github.com/ChristianDDinga/demoses-distributed-optimization) |
| (2/5) license                      | [![github license badge](https://img.shields.io/github/license/ChristianDDinga/demoses-distributed-optimization)](https://github.com/ChristianDDinga/demoses-distributed-optimization) |
| (3/5) community registry           | [![RSD](https://img.shields.io/badge/rsd-demoses_distibuted_optimization-00a3e3.svg)](https://www.research-software.nl/software/demoses_distibuted_optimization) [![workflow pypi badge](https://img.shields.io/pypi/v/demoses_distibuted_optimization.svg?colorB=blue)](https://pypi.python.org/project/demoses_distibuted_optimization/) |
| (4/5) citation                     | [![DOI](https://zenodo.org/badge/DOI/<replace-with-created-DOI>.svg)](https://doi.org/<replace-with-created-DOI>) |
| (5/5) checklist                    | [![workflow cii badge](https://bestpractices.coreinfrastructure.org/projects/<replace-with-created-project-identifier>/badge)](https://bestpractices.coreinfrastructure.org/projects/<replace-with-created-project-identifier>) |
| howfairis                          | [![fair-software badge](https://img.shields.io/badge/fair--software.eu-%E2%97%8F%20%20%E2%97%8F%20%20%E2%97%8F%20%20%E2%97%8F%20%20%E2%97%8B-yellow)](https://fair-software.eu) |
| **Other best practices**           | &nbsp; |
| Static analysis                    | [![workflow scq badge](https://sonarcloud.io/api/project_badges/measure?project=ChristianDDinga_demoses-distributed-optimization&metric=alert_status)](https://sonarcloud.io/dashboard?id=ChristianDDinga_demoses-distributed-optimization) |
| Coverage                           | [![workflow scc badge](https://sonarcloud.io/api/project_badges/measure?project=ChristianDDinga_demoses-distributed-optimization&metric=coverage)](https://sonarcloud.io/dashboard?id=ChristianDDinga_demoses-distributed-optimization) |
| Documentation                      | [![Documentation Status](https://readthedocs.org/projects/demoses-distributed-optimization/badge/?version=latest)](https://demoses-distributed-optimization.readthedocs.io/en/latest/?badge=latest) |
| **GitHub Actions**                 | &nbsp; |
| Build                              | [![build](https://github.com/ChristianDDinga/demoses-distributed-optimization/actions/workflows/build.yml/badge.svg)](https://github.com/ChristianDDinga/demoses-distributed-optimization/actions/workflows/build.yml) |
| Citation data consistency          | [![cffconvert](https://github.com/ChristianDDinga/demoses-distributed-optimization/actions/workflows/cffconvert.yml/badge.svg)](https://github.com/ChristianDDinga/demoses-distributed-optimization/actions/workflows/cffconvert.yml) |
| SonarCloud                         | [![sonarcloud](https://github.com/ChristianDDinga/demoses-distributed-optimization/actions/workflows/sonarcloud.yml/badge.svg)](https://github.com/ChristianDDinga/demoses-distributed-optimization/actions/workflows/sonarcloud.yml) |
| MarkDown link checker              | [![markdown-link-check](https://github.com/ChristianDDinga/demoses-distributed-optimization/actions/workflows/markdown-link-check.yml/badge.svg)](https://github.com/ChristianDDinga/demoses-distributed-optimization/actions/workflows/markdown-link-check.yml) |

## How to use demoses_distibuted_optimization

A distributed optimization approach based on ADMM for iteratively coordinating the interactions between electricity supply and demand agents until convergence (market equilibrium).

The project setup is documented in [project_setup.md](project_setup.md). Feel free to remove this document (and/or the link to this document) if you don't need it.

## Installation

To install demoses_distibuted_optimization from GitHub repository, do:

```console
git clone git@github.com:ChristianDDinga/demoses-distributed-optimization.git
cd demoses-distributed-optimization
python -m pip install .
```

## Documentation

Include a link to your project's full documentation here.

## Contributing

If you want to contribute to the development of demoses_distibuted_optimization,
have a look at the [contribution guidelines](CONTRIBUTING.md).

## Credits

This package was created with [Cookiecutter](https://github.com/audreyr/cookiecutter) and the [NLeSC/python-template](https://github.com/NLeSC/python-template).
