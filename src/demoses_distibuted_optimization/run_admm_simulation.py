import timeit
import pandas as pd
import yaml
from admm_main import admm_main
from define_common_parameters import define_common_parameters
from define_consumer_parameters import define_consumer_parameters
from define_EOM_parameters import define_EOM_parameters
from define_generator_parameters import define_generator_parameters
from define_results import define_results
from save_results import save_results


## 1. Read input data required to run the ADMM simulation
def read_config(config_file):
    """Read simulation configuration file."""
    with open(config_file, 'r') as file:
        config = yaml.safe_load(file)
    return config

ts = pd.read_csv('timeseries.csv', delimiter=';')
data = read_config('config.yaml')

print("############### Declare optimization models for all agents: start ####################")
print()
## 2. Construct abstract models for representative agents
agents = {}
agents["Gen"] = [id for id in data['Generators'].keys()]
agents["Cons"] = [id for id in data['Consumers'].keys()]
agents["all"] = agents['Gen'] + agents['Cons']  # all agents in the game
agents["eom"] = agents['Gen'] + agents['Cons']  # agents participating in the EOM

# Build general model with parameters common both to consumers and generatos
models_dict = {agent: define_common_parameters(agent, data) for agent in agents["all"]}

# # Print consumer models
for agent in agents['Cons']:
    define_consumer_parameters(agent=agent, model=models_dict[agent], data=data, ts=ts)

# Print generator models
for agent in agents['Gen']:
    define_generator_parameters(agent=agent, model=models_dict[agent], data=data, ts=ts)

## 3. Define parameters for markets and representative agents
number_of_agents = len(agents['eom'])  # calculate number of agents in each market
EOM = define_EOM_parameters(ts=ts, number_of_agents=number_of_agents)

print("############### Declare optimization models for all agents: done ####################")
print()
## 4. Implement ADMM method to calculate equilibrium
print("Find equilibrium solution: start ...")
print()
print("(Progress indicators on primal and dual residuals relative to tolerance: <1 indicates convergence)")
print()

primal_var_and_λ_EOM, admm_parameters = define_results(agents, data) # initialize structure for results dictionaries

## 5 . Run ADMM simulation
#  Profiling using timeit
start_time = timeit.default_timer()

admm_main(primal_var_and_λ_EOM, admm_parameters, EOM, models_dict, agents, data) # calculate equilibrium

end_time = timeit.default_timer()
admm_parameters["walltime"] = (end_time - start_time)  # wall time  # Calculate wall time in minutes and store in ADMM

print("Find equilibrium solution: done ...")
print()
print(f"Required iterations: {admm_parameters['n_iter']}")
print()
print(f"RP EOM: {admm_parameters['Residuals']['Primal']['EOM'][-1]}, \
    -- Tolerance: {admm_parameters['Tolerance']['EOM']}")
print(f"RD EOM: {admm_parameters['Residuals']['Dual']['EOM'][-1]}, -- Tolerance: {admm_parameters['Tolerance']['EOM']}")
print()

print("############### print processing results: start ###############")
save_results(EOM, admm_parameters, primal_var_and_λ_EOM, data, agents)
print("############### print processing results: done ################")
