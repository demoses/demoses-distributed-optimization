import timeit
from typing import Dict
import numpy as np
import pyomo.environ as pyo
from solve_consumer_agent import solve_consumer_agent
from solve_generator_agent import solve_generator_agent


def implement_admm_subroutine(
    agent: str,
    agents: Dict,
    primal_var_and_λ_EOM: Dict,
    admm_parameters: Dict,
    EOM: Dict,
    model: pyo.ConcreteModel,
    data: Dict,
) -> Dict:
    """This function implements the first step of ADMM which is the admm_subroutine.

    admm_subroutine obtains the latest values of ADMM paramters (λ_EOM, g_bar, ρ_EOM) from the previous optimization
    round, and updates them to new_λ_EOM, new_g_bar, and new_ρ_EOM respectively. Next, it then calls the
    solve_generator_agent and solve_consumer_agent functions, which instantiates new optimization problems for each
    agents by updating ADMM paramters to their new values. Finally, it runs all optimization problems again and appends
    the results to the respective dictionaries.
    """
    # Initialize total run time
    total_time = 0
    start_time = timeit.default_timer()
    # Get current ADMM parameters and update to latest values for next optimization round (common to all agents)
    new_λ_EOM = primal_var_and_λ_EOM['λ']['EOM'][-1]
    new_g_bar = primal_var_and_λ_EOM['g'][agent][-1] - (1 / (EOM['nAgents'] + 1)) \
        * admm_parameters['Imbalances']['EOM'][-1]
    new_ρ_EOM = admm_parameters['ρ']['EOM'][-1]

    # Prepare updated data in right format for instantiating agent optimization problems
    instance_data = {
        None: {
            'λ_EOM': dict(enumerate(new_λ_EOM)), # Convert time-indexed parameters to dict for declaration in pyomo
            'g_bar': dict(enumerate(new_g_bar)),
            'ρ_EOM': {None: new_ρ_EOM},
        }
    }
    update_time = timeit.default_timer() - start_time
    total_time += update_time
    start_time = timeit.default_timer()
    # Instantiate and solve agent new optimization problems
    if agent in agents['Gen']:
        instance_model = solve_generator_agent(model=model, data=data, instance_data=instance_data)
    elif agent in agents['Cons']:
        instance_model = solve_consumer_agent(model=model, data=data, instance_data=instance_data)
    else:
        print(f'Agent {agent} is not part of the game')
        return 0

    solve_time = timeit.default_timer() - start_time
    total_time += solve_time

    # Query results
    start_time = timeit.default_timer()
    optimal_decision_variable = np.array([pyo.value(instance_model.var_g[t]) for t in instance_model.time])
    primal_var_and_λ_EOM['g'][agent].append(optimal_decision_variable)
    query_time = timeit.default_timer() - start_time
    total_time += query_time

    # Record the total time in the TO dictionary
    TO = dict()
    TO['total_time'] = TO.get('total_time', 0) + total_time
    return TO

