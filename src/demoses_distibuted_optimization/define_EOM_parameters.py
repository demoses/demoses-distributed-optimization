import pandas as pd


def define_EOM_parameters(ts: pd.DataFrame, number_of_agents: int):
    """Define a EOM dictionary with load data from the timeseries DataFrame."""
    EOM = dict()
    EOM['D'] = ts.loc[:,'LOAD'].values
    EOM["nAgents"] = number_of_agents
    return EOM
