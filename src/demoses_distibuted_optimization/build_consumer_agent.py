import pyomo.environ as pyo


def build_consumer_agent(model: pyo.AbstractModel) -> pyo.AbstractModel:
    """Build optimization model of consumer agent."""
    # Declare optimization variables
    model.var_g = pyo.Var(model.time, name='demand', domain=pyo.Reals, initialize=0) # positive if feeds power to system
    # Declare objective function
    def consumption_cost_rule(model):
        return (
            -1 * sum(model.λ_EOM[t] * model.var_g[t] for t in model.time) +
            sum(model.ρ_EOM / 2 * (model.var_g[t] - model.g_bar[t])**2 for t in model.time)
        )
    model.objective_function = pyo.Objective(rule=consumption_cost_rule, sense=pyo.minimize)
    # Declare energy balance constraint
    def energy_balance_rule(model, t):
        return (model.var_g[t] <= model.pv_profile[t] - model.demand_profile[t])
    model.energy_balance = pyo.Constraint(model.time, rule=energy_balance_rule)

    return model
