import csv
import numpy as np
from pyprojroot import here


def save_results(EOM, admm_parameters, primal_var_and_λ_EOM, data, agents):
    """Save results from the simulation in new csv files."""
    # Create output folder
    output_folder = here('results')
    output_folder.mkdir(parents=True, exist_ok=True)
    # General ADMM performance results
    admm_performance_results = {
        "primal_residual": admm_parameters["Residuals"]["Primal"]["EOM"][-1],
        "dual_residual": admm_parameters["Residuals"]["Dual"]["EOM"][-1],
        "number_of_iterations": admm_parameters["n_iter"],
        "wall_time_seconds": admm_parameters["walltime"],
    }

    admm_performance_file = output_folder / "admm-performance-results.csv"
    with open(admm_performance_file, mode='w', newline='') as file:
        writer = csv.DictWriter(file, fieldnames=admm_performance_results.keys(), delimiter=',')
        writer.writeheader() # write column headers
        writer.writerow(admm_performance_results)

    # Generator and consumer agents' decision variables in each timestep
    # Power_output is positive for consumer agents if they feed power to the system
    power_output = np.array([primal_var_and_λ_EOM["g"][agt][-1] for agt in agents["eom"]])
    # Market equilibrium (market price and agent production/consumption schedules)
    market_equilibrium_results = [
        list(range(1, data["General"]["nTimesteps"] + 1)),
        primal_var_and_λ_EOM["λ"]["EOM"][-1],
        *power_output, # unpack nested list. # dimension = (number_of_Agents, timesteps)
        EOM["D"],
    ]

    market_equilibrium_file = output_folder / "market-equilibrium-results.csv"
    with open(market_equilibrium_file, mode='w', newline='') as file:
        writer = csv.writer(file, delimiter=',')
        writer.writerow(
            ["Timestep", "MarketPrice"] + [f"GenAgent{m}" for m in agents["Gen"]]
            + [f"ConAgent{m}" for m in agents["Cons"]] + ["SystemDemand"]
        )
        writer.writerows(zip(*market_equilibrium_results))
