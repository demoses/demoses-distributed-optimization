from typing import Dict
import numpy as np
import pandas as pd
import pyomo.environ as pyo


def define_generator_parameters(
    agent: str,
    model: pyo.AbstractModel,
    data: Dict,
    ts: pd.DataFrame,
) -> pyo.AbstractModel:
    """Define parmaters common to all generator agents."""
    # Declare generator cost function parameters (OPEX are parametrized as a/2 * g**2 + b * g)
    model.gen_cost_param_a = pyo.Param(
        name='gen_cost_param_a', initialize=data['Generators'][agent]['a'], mutable=False
    )
    model.gen_cost_param_b = pyo.Param(
        name='gen_cost_param_b', initialize=data['Generators'][agent]['b'], mutable=False
    )
    # Declare generator installed capacity parameter
    model.gen_installed_capacity = pyo.Param(
        name='gen_installed_capacity', initialize=data['Generators'][agent]['C'], mutable=False
    )
    # Get generator available capacity timeseries: installed capacity * availability factor
    if 'AF' in data['Generators'][agent].keys():
        available_capacity = model.gen_installed_capacity * ts.loc[:, data['Generators'][agent]["AF"]].values
    else:
        available_capacity = model.gen_installed_capacity * np.ones(data['General']["nTimesteps"])
    # Declare available capacity parameter
    available_capacity_dict = dict(enumerate(available_capacity))
    model.available_capacity = pyo.Param(
        model.time, name='available_capacity', initialize=available_capacity_dict, domain=pyo.Reals, mutable=False
    )

    return model
