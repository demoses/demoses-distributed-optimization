from typing import Dict
import numpy as np
import pyomo.environ as pyo
from build_consumer_agent import build_consumer_agent


def solve_consumer_agent(model: pyo.AbstractModel, data: Dict, instance_data: Dict = None) -> pyo.ConcreteModel:
    """Solve consumer agent's optimization problem."""
    number_of_timesteps = data["General"]["nTimesteps"]
    # First delete existing model components if they were previously instantiated
    if hasattr(model, 'var_g'):
        model.del_component(model.var_g)
    if hasattr(model, 'objective_function'):
        model.del_component(model.objective_function)
    if hasattr(model, 'energy_balance'):
        model.del_component(model.energy_balance)
    # Default instance data
    default_instance_data = {
        None: {
            'λ_EOM': dict(enumerate(np.zeros(number_of_timesteps))),
            'g_bar': dict(enumerate(np.zeros(number_of_timesteps))),
            'ρ_EOM': {None: 1},
        }
    }
    # Use default instance data if none is provided
    if instance_data is None:
        instance_data = default_instance_data
    # Reconstruct and instantiate model for next round of solving
    model = build_consumer_agent(model)
    model_instance = model.create_instance(data=instance_data)
    solver = pyo.SolverFactory('gurobi')
    solver.solve(model_instance)

    return model_instance
