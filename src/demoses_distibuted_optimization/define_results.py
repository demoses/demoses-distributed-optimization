import collections
from typing import Dict
from typing import Tuple
import numpy as np


def define_results(agents: Dict, data: Dict) -> Tuple[Dict, Dict]:
    """Get results from running ADMM simulations."""
    primal_var_and_λ_EOM = dict()
    primal_var_and_λ_EOM['g'] = dict()
    primal_var_and_λ_EOM['λ'] = dict()
    for agent in agents['eom']:
        # Set buffer size: max number of iterations for which data will be retained.
        #  collections.deque mimics julia's CircularBuffer.jl
        primal_var_and_λ_EOM['g'][agent] = collections.deque(maxlen=data['ADMM']['CircularBufferSize'])
        primal_var_and_λ_EOM['g'][agent].append(np.zeros(data['General']['nTimesteps']))

    primal_var_and_λ_EOM['λ']['EOM'] = collections.deque(maxlen=data['ADMM']['CircularBufferSize'])
    primal_var_and_λ_EOM['λ']['EOM'].append(np.zeros(data['General']['nTimesteps']))

    admm_parameters = dict()
    admm_parameters['Imbalances'] = dict()
    admm_parameters['Imbalances']['EOM'] = collections.deque(maxlen=data['ADMM']['CircularBufferSize'])
    admm_parameters['Imbalances']['EOM'].append(np.zeros(data['General']['nTimesteps']))

    admm_parameters['Residuals'] = dict()
    admm_parameters['Residuals']['Primal'] = dict()
    admm_parameters['Residuals']['Dual'] = dict()
    admm_parameters['Residuals']['Primal']['EOM'] = collections.deque(maxlen=data['ADMM']['CircularBufferSize'])
    admm_parameters['Residuals']['Primal']['EOM'].append(0.0)
    admm_parameters['Residuals']['Dual']['EOM'] = collections.deque(maxlen=data['ADMM']['CircularBufferSize'])
    admm_parameters['Residuals']['Dual']['EOM'].append(0.0)

    admm_parameters['Tolerance'] = dict()
    admm_parameters['Tolerance']['EOM'] = data['ADMM']['epsilon']

    admm_parameters['ρ'] = dict()
    admm_parameters['ρ']['EOM'] = collections.deque(maxlen=data['ADMM']['CircularBufferSize'])
    admm_parameters['ρ']['EOM'].append(data['ADMM']['rho_EOM'])

    admm_parameters['n_iter'] = 1
    admm_parameters['walltime'] = 0

    return primal_var_and_λ_EOM, admm_parameters
