import pandas as pd
import pyomo.environ as pyo
import yaml


def create_centralized_optimization_problem(data: dict, ts: pd.DataFrame) -> pyo.ConcreteModel:
    """Create optimization problem to solve the equilibrium problem in a centralized manner."""
    model = pyo.ConcreteModel(name='Centralized-optimization-problem')
    # General parmaters
    number_of_timesteps = data["General"]["nTimesteps"]
    total_consumers = data['General']['totConsumers']
    system_demand = ts.loc[:,'LOAD'].values  # system external demand (without consumer agents' demands)

    # Sets
    model.time = pyo.Set(initialize=list(range(number_of_timesteps)), name='timesteps')
    model.generators = pyo.Set(name='generators', initialize=list(data["Generators"]))
    model.consumers = pyo.Set(name='consumers', initialize=list(data["Consumers"]))

    # System external demand (besides consumer agents' demands)
    model.system_demand = pyo.Param(model.time, name='SystemDemand', initialize=dict(enumerate(system_demand)))

    # Consumer parameters
    ## Consumer individual demands
    def consumer_dem(model, con, t):
        share_of_consumer_type = data['Consumers'][con]['Share']
        total_consumers = data['General']['totConsumers']
        normalized_demand_profile = ts.loc[t, data['Consumers'][con]['D']]
        demand_profile = total_consumers * share_of_consumer_type * normalized_demand_profile
        return demand_profile
    model.demand_profile = pyo.Param(model.consumers, model.time, name='ConsumerDemands', initialize=consumer_dem)
    ## sum of demands for all consumer agents
    def all_con_agents_demand_timeseries_rule(model, t):
        return sum(model.demand_profile[con, t] for con in model.consumers)
    model.all_cons_agents_demand = pyo.Expression(model.time, initialize=all_con_agents_demand_timeseries_rule)
    ## Consumer pv profiles
    def consumer_pv_profile(model, con, t):
        share_of_consumer_type = data['Consumers'][con]['Share']
        pv_availability_factor = ts.loc[t, data['Consumers'][con]['PV_AF']]
        pv_capacity = data['Consumers'][con]['PV_cap']
        return total_consumers * share_of_consumer_type * pv_capacity * pv_availability_factor
    model.pv_profile = pyo.Param(model.consumers, model.time, name='PvProfiles', initialize=consumer_pv_profile)

    # Generator parameters
    generator_cost_param_a, generator_cost_param_b, generator_capacity = {}, {}, {}
    for generator_name, parameters in data['Generators'].items():
        generator_cost_param_a[generator_name] = parameters['a']
        generator_cost_param_b[generator_name] = parameters['b']
        generator_capacity[generator_name] = parameters['C']
    model.gen_cost_a = pyo.Param(model.generators, name='GenCost_a', initialize=generator_cost_param_a, mutable=False)
    model.gen_cost_b = pyo.Param(model.generators, name='GenCost_b', initialize=generator_cost_param_b, mutable=False)
    model.installed_cap = pyo.Param(model.generators, name='GenInsCap', initialize=generator_capacity, mutable=False)
    ## Generator available capacity
    def available_cap(model, gen, t):
        if 'AF' in data['Generators'][gen].keys():
            return model.installed_cap[gen] * ts.loc[t, data['Generators'][gen]["AF"]]
        else:
            return model.installed_cap[gen] * 1 # for conventional generators
    model.gen_capacity = pyo.Param(model.generators, model.time, name='Cap', initialize=available_cap, mutable=False)

    # Decision variables
    model.p_gen = pyo.Var(model.generators, model.time, name='generation', domain=pyo.NonNegativeReals, initialize=0)
    model.p_con = pyo.Var(model.consumers, model.time, name='consumption', domain=pyo.Reals, initialize=0)

    # Objective function (OPEX are parametrized as a/2 * Pg**2 + b * Pg)
    def generation_cost_rule(model):
        return (
            sum(model.gen_cost_a[g]/2 * model.p_gen[g, t]**2 for g in model.generators for t in model.time)
            + sum(model.gen_cost_b[g] * model.p_gen[g, t] for g in model.generators for t in model.time)
        )
    model.objective_function = pyo.Objective(rule=generation_cost_rule, sense=pyo.minimize)

    # Generator capacity limit constraint
    def capacity_limit_rule(model, gen, t):
        return model.p_gen[gen, t] <= model.gen_capacity[gen, t]
    model.capacity_limit = pyo.Constraint(model.generators, model.time, rule=capacity_limit_rule)

    # Consumer internal energy balance constraint
    def consumer_energy_balance_rule(model, con, t):
       return model.p_con[con, t] <=  model.pv_profile[con, t] - model.demand_profile[con, t]
    model.consumer_energy_balance = pyo.Constraint(model.consumers, model.time, rule=consumer_energy_balance_rule)

    # System energy balance constraint
    def system_energy_balance_rule(model, t):
        return (
            sum(model.p_gen[gen, t] for gen in model.generators) + sum(model.p_con[con, t] for con in model.consumers)
            == model.system_demand[t]
        )
    model.system_energy_balance = pyo.Constraint(model.time, rule=system_energy_balance_rule)

    return model


def solve_model():
    """Solve model and print results."""
    def read_config(config_file):
        with open(config_file, 'r') as file:
            config = yaml.safe_load(file)
        return config
    data = read_config('config.yaml')
    ts = pd.read_csv('timeseries.csv', delimiter=';')
    model = create_centralized_optimization_problem(data, ts)
    solver = pyo.SolverFactory('gurobi')
    model.dual = pyo.Suffix(direction=pyo.Suffix.IMPORT)
    solver.solve(model)
    print('############## Objective function value ###########')
    model.objective_function.display()
    print()
    print('############## Generator power output ###########')
    model.p_gen.display()
    print()
    print('############## Consumer consumption ###########')
    model.p_con.display()
    print()
    dual_values = []
    for t in model.time:
        dual_values.append(model.dual[model.system_energy_balance[t]])
    market_prices = pd.DataFrame(data={"market-price": dual_values}, index=model.time)
    market_prices.index.name = 'timesteps'
    print('############## Market prices ###########')
    print(market_prices)


if __name__ == "__main__":
    solve_model()
