import pyomo.environ as pyo


def build_generator_agent(model: pyo.AbstractModel) -> pyo.AbstractModel:
    """Build optimization model of generator agent."""
    # Declare optimization variables
    model.var_g = pyo.Var(model.time, name='generation', domain=pyo.NonNegativeReals, initialize=0)
    # Declare objective function  (OPEX are parametrized as a/2 * g**2 + b * g)
    def generation_cost_rule(model):
        return (
            sum((model.gen_cost_param_a/2)*model.var_g[t]**2 for t in model.time)
            + sum(model.gen_cost_param_b*model.var_g[t] for t in model.time)
            - sum(model.λ_EOM[t]*model.var_g[t] for t in model.time)
            + sum((model.ρ_EOM/2)*(model.var_g[t] - model.g_bar[t])**2 for t in model.time)
        )
    model.objective_function = pyo.Objective(rule=generation_cost_rule, sense=pyo.minimize)
    # Declare generator capacity limit constraint
    def capacity_limit_rule(model, t):
        return (model.var_g[t] <= model.available_capacity[t])
    model.capacity_limit = pyo.Constraint(model.time, rule=capacity_limit_rule)

    return model
