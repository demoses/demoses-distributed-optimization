from typing import Dict
import pandas as pd
import pyomo.environ as pyo


def define_consumer_parameters(agent: str, model: pyo.AbstractModel, data: Dict, ts: pd.DataFrame) -> pyo.AbstractModel:
    """Define parmaters common to all consumer agents."""
    # Extract consumer parameter values from data
    total_consumers = data['General']['totConsumers']
    share_of_agent = data['Consumers'][agent]['Share']
    # Demand
    normalized_demand_profile = ts.loc[:, data['Consumers'][agent]['D']].values
    demand_profile = total_consumers * share_of_agent * normalized_demand_profile
    # PV
    pv_availability_factor = ts.loc[:, data['Consumers'][agent]['PV_AF']].values
    pv_capacity = data['Consumers'][agent]['PV_cap']
    pv_profile = total_consumers * share_of_agent * pv_capacity * pv_availability_factor
    # Declare consumer optimization parameters (but do not construct yet)
    model.demand_profile = pyo.Param(
        model.time, name='demand_profile', initialize=dict(enumerate(demand_profile)), mutable=False
    )
    model.pv_profile = pyo.Param(model.time, name='pv_profile', initialize=dict(enumerate(pv_profile)), mutable=False)

    return model
