from typing import Dict


def update_rho(admm_parameters: Dict, iter: int) -> None:
    """Update penalty parameter ρ following Boyd et al.(2011), Page 20, Equation 3.13."""
    if admm_parameters["Residuals"]["Primal"]["EOM"][-1] > 2 * admm_parameters["Residuals"]["Dual"]["EOM"][-1]:
        admm_parameters["ρ"]["EOM"].append(min(1000, 1.1 * admm_parameters["ρ"]["EOM"][-1]))
    elif admm_parameters["Residuals"]["Dual"]["EOM"][-1] > 2 * admm_parameters["Residuals"]["Primal"]["EOM"][-1]:
        admm_parameters["ρ"]["EOM"].append((admm_parameters["ρ"]["EOM"][-1]) / 1.1)
