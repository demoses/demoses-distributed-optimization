import math
from typing import Dict
import numpy as np
from implement_admm_subroutine import implement_admm_subroutine
from tqdm import tqdm
from update_rho import update_rho


def admm_main(
    primal_var_and_λ_EOM: Dict,
    admm_parameters: Dict,
    EOM: Dict,
    models_dict: Dict,
    agents: Dict,
    data: Dict
) -> None:
    """Implement main loop of ADMM algorithm."""
    convergence = 0
    iterations = tqdm(range(1, data["ADMM"]["max_iter"] + 1))
    for iter in iterations:
        if convergence == 0:
            for agent in agents['eom']:
                implement_admm_subroutine(
                    agent,agents, primal_var_and_λ_EOM, admm_parameters, EOM, models_dict[agent], data,
                )
            # Imbalances
            all_agents_decision_variables = np.array([primal_var_and_λ_EOM["g"][agent][-1] for agent in agents['eom']])
            imbalances_per_timestep = np.sum(all_agents_decision_variables, axis=0) - EOM["D"]
            admm_parameters["Imbalances"]["EOM"].append(imbalances_per_timestep)
            # Primal residuals
            squared_values_of_primal_residuals = np.array(
                [imbalance**2 for imbalance in admm_parameters["Imbalances"]["EOM"][-1]]
            )
            sum_of_squared_primal_residuals = np.sum(squared_values_of_primal_residuals)
            primal_residuals = math.sqrt(sum_of_squared_primal_residuals)
            admm_parameters["Residuals"]["Primal"]["EOM"].append(primal_residuals)
            # Dual residuals
            if iter > 1:
                squared_dual_residual_for_all_agents = 0
                for agent in agents['eom']:
                    term_to_subtract_current_iteration = np.sum(
                        np.array([primal_var_and_λ_EOM["g"][agt][-1] for agt in agents['eom']]), axis=0
                    ) / (EOM["nAgents"] + 1)
                    term_to_subtract_previous_iteration = np.sum(
                        np.array([primal_var_and_λ_EOM["g"][agt][-2] for agt in agents['eom']]), axis=0
                    ) / (EOM["nAgents"] + 1)
                    primal_diff_current_iter = primal_var_and_λ_EOM["g"][agent][-1] - term_to_subtract_current_iteration
                    primal_diff_prev_iter = primal_var_and_λ_EOM["g"][agent][-2] - term_to_subtract_previous_iteration
                    dual_residual_per_agent = (
                        admm_parameters["ρ"]["EOM"][-1] * (primal_diff_current_iter - primal_diff_prev_iter)
                    )
                    # Apply L-2 norm: start (first square all terms in the array, next add them altogether)
                    sum_of_squared_dual_residual_for_single_agent = np.sum((dual_residual_per_agent** 2))
                    squared_dual_residual_for_all_agents += sum_of_squared_dual_residual_for_single_agent
                # Apply L-2 norm: end (finally take sqrt before appending to list)
                dual_residual = math.sqrt(squared_dual_residual_for_all_agents)
                admm_parameters["Residuals"]["Dual"]["EOM"].append(dual_residual)
            # Price updates
            previous_market_price = primal_var_and_λ_EOM["λ"]["EOM"][-1]
            adjustment_term  =  (admm_parameters["ρ"]["EOM"][-1] / 10) * admm_parameters["Imbalances"]["EOM"][-1]
            new_market_price = previous_market_price - adjustment_term
            primal_var_and_λ_EOM["λ"]["EOM"].append(new_market_price)
            # Update ρ-values
            update_rho(admm_parameters, iter)
            # Progress bar
            iterations.set_description(
                f"Primal residual: {admm_parameters['Residuals']['Primal']['EOM'][-1]:.3f} \
                -- Dual residual: {admm_parameters['Residuals']['Dual']['EOM'][-1]:.3f}"
            )
            # Check convergence: primal and dual satisfy tolerance
            primal_stopping = admm_parameters["Residuals"]["Primal"]["EOM"][-1] <= admm_parameters["Tolerance"]["EOM"]
            dual_stopping = admm_parameters["Residuals"]["Dual"]["EOM"][-1] <= admm_parameters["Tolerance"]["EOM"]
            if primal_stopping and dual_stopping:
                convergence = 1
                print("############## print final market prices: start #################")
                print(primal_var_and_λ_EOM["λ"]["EOM"][-1])
                print("############## print final market prices: done #################")
            # store number of iterations
            admm_parameters["n_iter"] = iter
