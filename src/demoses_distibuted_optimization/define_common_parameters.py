from typing import Dict
import numpy as np
import pyomo.environ as pyo


def define_common_parameters(agent: str,  data: Dict) -> pyo.AbstractModel:
    """Define common parameters to all optimization problems."""
    # Use Abstract instead of Concrete model
    model = pyo.AbstractModel(name=f'Optimization-problem-of-agent-###{agent}###')
    # Declare and set (but do not construct yet)
    number_of_timesteps = data["General"]["nTimesteps"]
    model.time =  pyo.Set(initialize=list(range(number_of_timesteps)), name='timesteps')
    # print(model.time.is_constructed())
    # Declare common optimization parameters related to the EOM (but do not construct yet)
    λ_EOM_initial = dict(enumerate(np.zeros(number_of_timesteps)))
    g_bar_initial = dict(enumerate(np.zeros(number_of_timesteps)))
    ρ_EOM_initial = data["ADMM"]["rho_EOM"]
    model.λ_EOM = pyo.Param(model.time, name='λ_EOM', initialize=λ_EOM_initial, mutable=True)
    model.g_bar = pyo.Param(model.time, name='g_bar', initialize=g_bar_initial, mutable=True)
    model.ρ_EOM = pyo.Param(name='ρ_EOM', initialize=ρ_EOM_initial, mutable=True)

    return model
